# @pushrocks/smartssh
setups SSH quickly and in a painless manner

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@pushrocks/smartssh)
* [gitlab.com (source)](https://gitlab.com/pushrocks/smartssh)
* [github.com (source mirror)](https://github.com/pushrocks/smartssh)
* [docs (typedoc)](https://pushrocks.gitlab.io/smartssh/)

## Status for master

Status Category | Status Badge
-- | --
GitLab Pipelines | [![pipeline status](https://gitlab.com/pushrocks/smartssh/badges/master/pipeline.svg)](https://lossless.cloud)
GitLab Pipline Test Coverage | [![coverage report](https://gitlab.com/pushrocks/smartssh/badges/master/coverage.svg)](https://lossless.cloud)
npm | [![npm downloads per month](https://badgen.net/npm/dy/@pushrocks/smartssh)](https://lossless.cloud)
Snyk | [![Known Vulnerabilities](https://badgen.net/snyk/pushrocks/smartssh)](https://lossless.cloud)
TypeScript Support | [![TypeScript](https://badgen.net/badge/TypeScript/>=%203.x/blue?icon=typescript)](https://lossless.cloud)
node Support | [![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
Code Style | [![Code Style](https://badgen.net/badge/style/prettier/purple)](https://lossless.cloud)
PackagePhobia (total standalone install weight) | [![PackagePhobia](https://badgen.net/packagephobia/install/@pushrocks/smartssh)](https://lossless.cloud)
PackagePhobia (package size on registry) | [![PackagePhobia](https://badgen.net/packagephobia/publish/@pushrocks/smartssh)](https://lossless.cloud)
BundlePhobia (total size when bundled) | [![BundlePhobia](https://badgen.net/bundlephobia/minzip/@pushrocks/smartssh)](https://lossless.cloud)
Platform support | [![Supports Windows 10](https://badgen.net/badge/supports%20Windows%2010/yes/green?icon=windows)](https://lossless.cloud) [![Supports Mac OS X](https://badgen.net/badge/supports%20Mac%20OS%20X/yes/green?icon=apple)](https://lossless.cloud)

## Usage

```javascript
var smartssh = require('smartssh');
var sshInstance = new smartssh.sshInstance({
  sshDir: '/some/path/.ssh', // the standard ssh directory, optional, defaults to "~./.ssh"
  sshSync: true, // sync ssh this instance will represent the status of an ssh dir if set to true;
});

sshInstance.addKey(
  new smartssh.sshKey({
    private: 'somestring',
    public: 'somestring', // optional
    host: 'github.com',
    encoding: 'base64', // optional, defaults to "utf8", can be "utf8" or "base64", useful for reading ssh keys from environment variables
  })
);

sshInstance.removeKey(sshInstance.getKey('github.com')); // removes key for host "github.com" is present

sshInstance.createKey({
  host: 'gitlab.com', // returns new key in the form sshKey, read more about the sshKey class below
});

sshInstance.getKey({
  // returns ssh key in the form sshKey, read more about the sshKey class below
  host: 'github.com',
});

sshInstance.getKeys(); // returns array of all available getKeys. Each key is in form of class sshKey
```


## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
